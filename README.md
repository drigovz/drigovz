<!-- <center>
    <table>
        <tr>
            <td><img width="380px" align="left" src="https://github-readme-stats.vercel.app/api/top-langs/?username=drigovz&hide=html&layout=compact&theme=buefy&title_color=814AC1&bg_color=0D1117&text_color=FFFFFF&hide_border=true" /></td>
            <td><img width="380px" align="left" src="https://github-readme-stats.vercel.app/api?username=drigovz&theme=buefy&title_color=814AC1&bg_color=0D1117&text_color=FFFFFF&hide_border=true"/></td>
        </tr>   
    </table>
</center>  

<hr /> -->

### Hi there <img src="https://github.com/drigovz/drigovz/blob/main/svg/hello.gif" width="25px" />
<!--👋-->

My name is Rodrigo Vaz and I'm a developer/programmer more focused on the development applications for Web. With a focus on Back-end and Front-end too. I'm also very fond of databases and I work with databases such as Microsoft SQL Server, PostgreSQL and MySQL as well like MongoDB and Redis for caching.

Most of my applications, whether professional projects or personal projects, use Microsoft technologies: **.NET Core**, **SQL Server** and **C#**. However, I work too with technologies such as Node.js, React.js, Typescript, Javascript, Docker, Docker-Compose, PowerShell Script, Bash Script and Linux Systems.

I always try to learn new technologies and study and update my knowledges, always focusing on clean and readable codes using Clean Code and Design Patterns.

#### Languages & Tools 🛠

![csharp](https://img.shields.io/badge/-csharp-05122A?style=flat&color=purple&logo=csharp)&nbsp;![.net](https://img.shields.io/badge/-.net-05122A?style=flat&color=purple&logo=.net)&nbsp;![visualstudio](https://img.shields.io/badge/-visualstudio-05122A?style=flat&color=purple&logo=visualstudio)&nbsp;![microsoftsqlserver](https://img.shields.io/badge/-sqlserver-05122A?style=flat&color=red&logo=microsoftsqlserver)&nbsp;![docker](https://img.shields.io/badge/-docker-05122A?style=flat&color=004080&logo=docker)&nbsp;![visualstudiocode](https://img.shields.io/badge/-visualstudiocode-05122A?style=flat&color=blue&logo=visualstudiocode)&nbsp;![redis](https://img.shields.io/badge/-redis-05122A?style=flat&color=c2c2a3&logo=redis)&nbsp;![powershell](https://img.shields.io/badge/-powershell-05122A?style=flat&color=264d73&logo=powershell)&nbsp;![windowsterminal](https://img.shields.io/badge/-windowsterminal-05122A?style=flat&color=blue&logo=windowsterminal)&nbsp;![github](https://img.shields.io/badge/-github-05122A?style=flat&color=blue&logo=github)&nbsp;![git](https://img.shields.io/badge/-git-05122A?style=flat&color=c2c2a3&logo=git)&nbsp;

<!-- ![githubactions](https://img.shields.io/badge/-githubactions-05122A?style=flat&color=white&logo=githubactions)&nbsp; -->
<!-- ![windows](https://img.shields.io/badge/-windows-05122A?style=flat&color=blue&logo=windows)&nbsp; -->
<!-- ![nodejs](https://img.shields.io/badge/-nodejs-05122A?style=flat&color=green$logo=nodejs&logo=node.js)&nbsp;![typescript](https://img.shields.io/badge/-typescript-05122A?style=flat&color=white&logo=typescript)&nbsp;![javascript](https://img.shields.io/badge/-javascript-05122A?style=flat&color=0d1017&logo=javascript)&nbsp; -->
<!-- ![bash](https://img.shields.io/badge/-bash-05122A?style=flat&color=0d1017&logo=gnubash)&nbsp; -->
<!-- ![linux](https://img.shields.io/badge/-linux-05122A?style=flat&color=0d1017&logo=linux)&nbsp; -->
<!-- ![kubernetes](https://img.shields.io/badge/-kubernetes-05122A?style=flat&color=0d1017&logo=kubernetes)&nbsp; -->
<!-- ![postgresql](https://img.shields.io/badge/-postgresql-05122A?style=flat&color=0d1017&logo=postgresql)&nbsp;
![mysql](https://img.shields.io/badge/-mysql-05122A?style=flat&color=0d1017&logo=mysql)&nbsp;
![mongodb](https://img.shields.io/badge/-mongodb-05122A?style=flat&color=green&logo=mongodb)&nbsp; -->


#### Where to find me? 🔍

<div style="text-align: left;">
<table style="border:0px">
	<td style="border:0px">
		<a href="https://twitter.com/drigovz" target="_blank">
			<img width="20px" align="center" style="display: inline-block; cursor: normal;" src="svg/logo_twitter.png" />
		</a>
	</td>
	<td style="border:0px">
		<a href="https://www.instagram.com/drigovz/" target="_blank">
		<img width="20px" align="center" style="display: inline; cursor: normal; margin-right: 10px;" src="svg/logo_instagram.png" />
	</a>
	</td>
	<td style="border:0px">
		<a href="https://www.linkedin.com/in/rodrigo-vaz-del-pino/" target="_blank">
		<img width="20px" align="center" style="display: inline-block; cursor: normal; margin-right: 10px;" src="svg/logo_linkedin.png" />
	</a>
	</td>
	<td style="border:0px">
	<a href="mailto:rodrigodp2014@gmail.com">
		<img width="20px" align="center" style="display: inline-block; cursor: normal;" src="svg/logo_gmail.png" />
	</a>
	</td>
</table>
</div>

<hr />
<p>
    <img src="https://komarev.com/ghpvc/?username=drigovz&color=blue&style=flat" />
</p>
